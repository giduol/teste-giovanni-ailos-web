package mapearElementos;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UsuarioBloqueadoElementos {

    @FindBy(id = "user-name")
    public WebElement user;

    @FindBy(id = "password")
    public WebElement password;

    @FindBy(id = "login-button")
    public WebElement botaoLogin;

    @FindBy(css = "#login_button_container > div > form > div.error-message-container.error > h3")
    public WebElement mensagemErro;

}
