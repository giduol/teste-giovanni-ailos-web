package mapearElementos;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ValidarValoresElementos {

    @FindBy(id = "user-name")
    public WebElement user;

    @FindBy(id = "password")
    public WebElement password;

    @FindBy(id = "login-button")
    public WebElement botaoLogin;

    @FindBy(id = "add-to-cart-sauce-labs-backpack")
    public WebElement mochila;

    @FindBy(xpath = "//*[@id=\"inventory_container\"]/div/div[1]/div[2]/div[2]/div")
    public WebElement valorMochila;

    @FindBy(id = "add-to-cart-sauce-labs-bike-light")
    public WebElement luzBicicleta;

    @FindBy(xpath = "//*[@id=\"inventory_container\"]/div/div[2]/div[2]/div[2]/div")
    public WebElement valorLuzBicicleta;

    @FindBy(id = "add-to-cart-sauce-labs-bolt-t-shirt")
    public WebElement camisetaBolt;

    @FindBy(xpath = "//*[@id=\"inventory_container\"]/div/div[3]/div[2]/div[2]/div")
    public WebElement valorCamisetaBolt;

    @FindBy(id = "add-to-cart-sauce-labs-fleece-jacket")
    public WebElement jaquetaLa;

    @FindBy(xpath = "//*[@id=\"inventory_container\"]/div/div[4]/div[2]/div[2]/div")
    public WebElement valorJaquetaLa;

    @FindBy(id = "add-to-cart-sauce-labs-onesie")
    public WebElement macacao;

    @FindBy(xpath = "//*[@id=\"inventory_container\"]/div/div[5]/div[2]/div[2]/div")
    public WebElement valorMacacao;

    @FindBy(id = "add-to-cart-test.allthethings()-t-shirt-(red)")
    public WebElement camiseta;

    @FindBy(xpath = "//*[@id=\"inventory_container\"]/div/div[6]/div[2]/div[2]/div")
    public WebElement valorCamiseta;

    @FindBy(id = "shopping_cart_container")
    public WebElement carrinho;

    @FindBy(id = "checkout")
    public WebElement checkout;

    @FindBy(id = "first-name")
    public WebElement firstName;

    @FindBy(id = "last-name")
    public WebElement lastName;

    @FindBy(id = "postal-code")
    public WebElement postalCode;

    @FindBy(id = "continue")
    public WebElement botaoContinue;

    @FindBy(xpath = "//*[@id=\"checkout_summary_container\"]/div/div[2]/div[5]")
    public WebElement subtotal;

    @FindBy(id = "finish")
    public WebElement botaoFinish;


}
