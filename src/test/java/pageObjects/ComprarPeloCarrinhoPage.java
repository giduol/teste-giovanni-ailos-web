package pageObjects;

import mapearElementos.ComprarPeloCarrinhoElementos;
import org.openqa.selenium.support.PageFactory;
import utils.Web;

public class ComprarPeloCarrinhoPage extends ComprarPeloCarrinhoElementos {

    public ComprarPeloCarrinhoPage() {
        PageFactory.initElements(Web.getCurrentDriver(), this);}

    public void incluirnome(String nome) {user.sendKeys(nome);}

    public void incluirsenha(String senha) {password.sendKeys(senha);}

    public void clicarLogin() {botaoLogin.click();}

    public void clicarMochila() {mochila.click();}

    public void clicarCamiseta() {camiseta.click();}

    public void clicarCarrinho() {carrinho.click();}

    public void clicarCheckout() {checkout.click();}

    public void incluirPrimeiroNome(String primeiroNome) {firstName.sendKeys(primeiroNome);}

    public void incluirUltimoNome(String ultimoNome) {lastName.sendKeys(ultimoNome);}

    public void incluirPostalCode(String cep) {postalCode.sendKeys(cep);}

    public void clicarContinue() {botaoContinue.click();}

    public void clicarFinish() {botaoFinish.click();}

    public String mensagemFinal() {return mensagemFinal.getText();}
}
