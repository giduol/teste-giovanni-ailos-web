package pageObjects;

import mapearElementos.UsuarioBloqueadoElementos;
import org.openqa.selenium.support.PageFactory;
import utils.Web;

public class UsuarioBloqueadoPage extends UsuarioBloqueadoElementos {

    public UsuarioBloqueadoPage() {
        PageFactory.initElements(Web.getCurrentDriver(), this);}

    public void incluirnome(String nome) {user.sendKeys(nome);}

    public void incluirsenha(String senha) {password.sendKeys(senha);}

    public void clicarLogin() {botaoLogin.click();}

    public String mensagemErro() {return mensagemErro.getText();}
}
