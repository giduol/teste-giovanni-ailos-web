package pageObjects;

import mapearElementos.ValidarValoresElementos;
import org.openqa.selenium.support.PageFactory;
import utils.Web;

public class ValidarValoresPage extends ValidarValoresElementos {

    public ValidarValoresPage() {
        PageFactory.initElements(Web.getCurrentDriver(), this);}

    public void incluirnome(String nome) {user.sendKeys(nome);}

    public void incluirsenha(String senha) {password.sendKeys(senha);}

    public void clicarLogin() {botaoLogin.click();}

    public void clicarMochila() {mochila.click();}

    public String valorMochila() {return valorMochila.getText();}

    public void clicarLuzBicicleta() {luzBicicleta.click();}

    public String valorLuzBicicleta() {return valorLuzBicicleta.getText();}

    public void clicarCamisetaBolt() {camisetaBolt.click();}

    public String valorCamisetaBolt() {return valorCamisetaBolt.getText();}

    public void clicarJaquetaLa() {jaquetaLa.click();}

    public String valorJaquetaLa() {return valorJaquetaLa.getText();}

    public void clicarMacacao() {macacao.click();}

    public String valorMacacao() {return valorMacacao.getText();}

    public void clicarCamiseta() {camiseta.click();}

    public String valorCamiseta() {return valorCamiseta.getText();}

    public void clicarCarrinho() {carrinho.click();}

    public void clicarCheckout() {checkout.click();}

    public void incluirPrimeiroNome(String primeiroNome) {firstName.sendKeys(primeiroNome);}

    public void incluirUltimoNome(String ultimoNome) {lastName.sendKeys(ultimoNome);}

    public void incluirPostalCode(String cep) {postalCode.sendKeys(cep);}

    public void clicarContinue() {botaoContinue.click();}

    public String valorTotal() {return subtotal.getText();}

    public void clicarFinish() {botaoFinish.click();}
}
