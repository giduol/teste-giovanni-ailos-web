package testcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.ComprarPeloCarrinhoPage;
import utils.BaseTest;

public class ComprarPeloCarrinho extends BaseTest {

    @Test()
    public void comprarPeloCarrinho() {
        ComprarPeloCarrinhoPage compra = new ComprarPeloCarrinhoPage();
        compra.incluirnome("standard_user");
        compra.incluirsenha("secret_sauce");
        compra.clicarLogin();
        compra.clicarMochila();
        compra.clicarCamiseta();
        compra.clicarCarrinho();
        compra.clicarCheckout();
        compra.incluirPrimeiroNome("Giovanni");
        compra.incluirUltimoNome("Oliveira");
        compra.incluirPostalCode("91300-000");
        compra.clicarContinue();
        compra.clicarFinish();
        Assert.assertEquals("THANK YOU FOR YOUR ORDER", compra.mensagemFinal());
        System.out.println("Mensagem compra efetuada com sucesso: " + compra.mensagemFinal());
    }
}