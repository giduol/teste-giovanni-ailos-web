package testcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.ValidarValoresPage;
import utils.BaseTest;

public class ValidarValores extends BaseTest {

    @Test()
    public void validarValores() {
        Double valorTotal = Double.valueOf(0);
        ValidarValoresPage compra = new ValidarValoresPage();
        compra.incluirnome("standard_user");
        compra.incluirsenha("secret_sauce");
        compra.clicarLogin();
        compra.clicarMochila();
        valorTotal = valorTotal + Double.valueOf(compra.valorMochila().substring(1, compra.valorMochila().length()));
        compra.clicarLuzBicicleta();
        valorTotal = valorTotal + Double.valueOf(compra.valorLuzBicicleta().substring(1, compra.valorLuzBicicleta().length()));
        compra.clicarCamisetaBolt();
        valorTotal = valorTotal + Double.valueOf(compra.valorCamisetaBolt().substring(1, compra.valorCamisetaBolt().length()));
        compra.clicarJaquetaLa();
        valorTotal = valorTotal + Double.valueOf(compra.valorJaquetaLa().substring(1, compra.valorJaquetaLa().length()));
        compra.clicarMacacao();
        valorTotal = valorTotal + Double.valueOf(compra.valorMacacao().substring(1, compra.valorMacacao().length()));
        compra.clicarCamiseta();
        valorTotal = valorTotal + Double.valueOf(compra.valorCamiseta().substring(1, compra.valorCamiseta().length()));
        compra.clicarCarrinho();
        compra.clicarCheckout();
        compra.incluirPrimeiroNome("Giovanni");
        compra.incluirUltimoNome("Oliveira");
        compra.incluirPostalCode("91300-000");
        compra.clicarContinue();
        Assert.assertEquals(valorTotal.toString(), compra.valorTotal().substring(13, compra.valorTotal().length()));
        System.out.println("Valor total: " + compra.valorTotal().substring(13, compra.valorTotal().length()));
        compra.clicarFinish();
    }
}







